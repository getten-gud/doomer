extends Node

signal server_started
signal server_stopped

signal add_client(client)
signal remove_client(client)

signal add_player(player)
signal remove_player(player)

signal add_admin(id)

signal add_map(map)
signal remove_map(map)

signal output(scope, text)

var name_generator = preload("res://server/name_generator.gd")

var r = {
	"player":preload("res://server/player.tscn")
}


var manifest

var loaded_maps = []

var players_connected = []
var admins_connected = []

var clients_by_id = {}
var players_by_nickname = {}

var websocket

func _ready():
	load_all_maps()
	pass
func _process(delta):

	if websocket != null and websocket.is_listening(): # is_listening is true when the server is active and listening
		websocket.poll();	


func start_websocket_server(port):
	websocket = WebSocketServer.new();
	
	var _error = get_tree().connect("network_peer_connected", self, "_player_connected")
	_error = get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	
	websocket.listen(port, PoolStringArray(), true);
	
	get_tree().set_network_peer(websocket);
	emit_signal("server_started")
	emit_signal("output", "global", "[b]Server Started on port " + String(port) + "[/b] \n Server is Listening: " + String(websocket.is_listening()))
	if not websocket.is_listening():
		stop_server()

func stop_server():
	get_tree().network_peer.stop()
	emit_signal("server_stopped")
	emit_signal("output", "global", "[b]Server Stopped.[/b]")
	
# Comes from a client requesting a map on initial connection.
remote func request_map(character, nickname, code):
	var id = get_tree().get_rpc_sender_id()
	
	if players_by_nickname.get(nickname) != null:
		rpc_id(id, "bad_nickname", "Nickname is already taken.")
		return
	
	var player = clients_by_id[id]
	player.set_name(String(id))
	player.set_details(character, nickname, code)	
	#assign_player_to_map(player)
	players_by_nickname[nickname] = player
	
	#if map != null:
	#	rpc_id(id, "init_map", map.map_data)

remote func register_as_admin(secret):
	var id = get_tree().get_rpc_sender_id()	
	if(secret == secret):
		clients_by_id[id].data.admin = true
		admins_connected.push_back(clients_by_id[id])
		emit_signal("add_admin", id)
		

func _player_connected(id):
	var player_instance = r.player.instance()
	player_instance.init(id)	
	clients_by_id[id] = player_instance
	players_connected.push_back(player_instance)
	emit_signal("add_client", id)
	emit_signal("output", "global", "Player by ID [b]" + String(id) + "[/b] connected.")
	player_instance.set_network_master(id)

	
func _player_disconnected(id):
	var player = clients_by_id[id]	
	clients_by_id[id].disconnect_from_game()
	emit_signal("remove_client", id)
	
	remove_player(id)
		
	emit_signal("output", "global", "Player by ID [b]" + String(id) + "[/b] disconnected.")	

func remove_player(id):
	var player = clients_by_id[id]
	if players_by_nickname.get(player.data.nickname):
		admins_connected.erase(player)
		var map = player.map
		if map != null:
			map.remove_player(player)
		emit_signal("remove_player", player.data.nickname)
		players_by_nickname.erase(player.data.nickname)
	clients_by_id[id].queue_free()
	clients_by_id.erase(id)
	pass

func load_all_maps():
	var save_game = File.new()
	if not save_game.file_exists("user://manifest.save"):
		manifest = {"maps":[]}
		save_manifest(manifest)
		return # Error! We don't have a save to load.
	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://manifest.save", File.READ)
	manifest = parse_json(save_game.get_line())
	for map in manifest.maps:
		load_map(map)

func save_manifest(manifest_data):
	var save_game = File.new()
	save_game.open("user://manifest.save", File.WRITE)
	
		# Store the save dictionary as a new line in the save file.
	save_game.store_line(to_json(manifest_data))
	save_game.close()
	
remote func request_new_map(map_name):
	var id = get_tree().get_rpc_sender_id()
	if clients_by_id[id].data.admin:
		create_new_map(map_name)

func create_new_map(map_name):
	var new_map = preload("res://server/game_map.tscn").instance()
	new_map.set_name(map_name)
	new_map.init(map_name)
	manifest.maps.push_back(map_name)
	save_manifest(manifest)
	loaded_maps.push_back(new_map)
	add_child(new_map)
	emit_signal("add_map", map_name)
	return new_map
	
func load_map(map_name):
	var new_map = preload("res://server/game_map.tscn").instance()
	new_map.set_name(map_name)
	new_map.init(map_name)
	loaded_maps.push_back(new_map)
	add_child(new_map)
	emit_signal("add_map", map_name)
	return new_map
	
func delete_map(map_name):
	manifest.maps.erase(map_name)
	save_manifest(manifest)
	
	for map in loaded_maps:
		if map.map_data.header.map_name == map_name:
			loaded_maps.erase(map)
			map.queue_free()
	
	var dir = Directory.new()
	dir.remove("user://" + map_name + ".world")
	emit_signal("remove_map", map_name)
	
func assign_player_to_map(player, map):
	player.map = map
	
	map.add_player(player)
	
	emit_signal("add_player", player.data.nickname)
	emit_signal("output", "global", "Player [b]" + player.data.nickname + "[/b] has been added to map [b]" + map.name + "[/b]")
	#choice_map.node.get_node("players").add_child(player)



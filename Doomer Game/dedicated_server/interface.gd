extends Node

onready var server = get_node('../')

onready var oview = {
	"status":get_node("Control Panel/contents/Server Status/output"),
	"clients":get_node("Control Panel/contents/HBoxContainer/Column 1/Clients/output"),
	"players":get_node("Control Panel/contents/HBoxContainer/Column 1/Players/output"),
	"maps":get_node("Control Panel/contents/HBoxContainer/Column 1/Maps/output"),
	"port":get_node("Control Panel/contents/Server Controls/SpinBox"),
	"start":get_node("Control Panel/contents/Server Controls/Start"),
	"stop":get_node("Control Panel/contents/Server Controls/Stop")
}

onready var clients = {
	"window":get_node("Connections Manager"),
	"list":get_node("Connections Manager/Contents/ItemList")
}

onready var connections = get_node("Connections Manager")
onready var players = get_node("Players Manager")
onready var maps = get_node("Maps Manager")

onready var output = get_node("server output")
onready var outputs = {
	"global":output.get_node("global")
}

# Called when the node enters the scene tree for the first time.
func _ready():
	server.connect("server_started", self, "_server_started")
	server.connect("server_stopped", self, "_server_stopped")
	
	server.connect("add_client", self, "_add_client")
	server.connect("remove_client", self, "_remove_client")
	
	server.connect("add_player", self, "_add_player")
	server.connect("remove_player", self, "_remove_player")
	
	server.connect("add_map", self, "_add_map")
	server.connect("remove_map", self, "_remove_map")
	
	server.connect("output", self, '_handle_output')

	server.connect("add_admin", self, "send_admin_data")
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _start_server():
	server.start_websocket_server(oview.port.value)
	
func _stop_server():
	server.stop_server()

func _server_started():
	oview.status.text = "running"
	oview.clients.text = "0"
	oview.players.text = "0"
	oview.maps.text = "0"
	oview.port.editable = false
	oview.start.disabled = true
	oview.stop.disabled = false
	
func _server_stopped():
	oview.status.text = "stopped"
	oview.clients.text = "0"
	oview.players.text = "0"
	oview.maps.text = "0"
	oview.port.editable = false
	oview.start.disabled = false
	oview.stop.disabled = true
	
func _handle_output(scope, message):
	if(outputs.get(scope) == null):
		var output_text = RichTextLabel.instance()
		output_text.set_name(scope)
		output.add_child(output_text)
	outputs[scope].append_bbcode("\n" + message)

func _add_client(client):
	connections.push_connection(client)

func _remove_client(client):
	connections.remove_connection(client)
	
func _add_player(player):
	players.push_player(player)
	
func _remove_player(player):
	players.remove_player(player)
	
func _add_map(map):
	maps.push_map(map)
	for player in server.admins_connected:
		rpc_id(player.data.id, "map_added", map)

func _remove_map(map):
	maps.remove_map(map)
	for player in server.admins_connected:
		rpc_id(player.data.id, "map_removed", map)

func send_admin_data(id):
	for map in server.manifest.maps:
		rpc_id(id, "map_added", map)
		
remote func remove_map(map):
	server.delete_map(map)

remote func get_map_details():
	pass

extends Node

const prefixes = ["ginyu", "cheesy", "sneaky", "robotic", "unreal", "totally-legit", "not"]

const suffixes = ["fighters", "den", "snakes", "sneakers", "alphabet factory", "notary public"]

static func generate_map_name():
	randomize()
	return prefixes[randi()%prefixes.size()] + " " + suffixes[randi()%suffixes.size()]

extends "res://scripts/oriented_object.gd"

var data = {
	"nickname":null,
	"character":null,
	"hp":100,
	"ammo":50,
	"position":{"x":0,"y":0},
	"admin":false,
	"id":null
}

var map = null

func init(id):
	set_name(String(id))
	data.id = id

func set_details(character, nickname, save_code = null):
	data.nickname = nickname
	data.character = character
	

remote func fire():
	pass

# Called when the node enters the scene tree for the first time.

func disconnect_from_game():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends Node


# Declare member variables here. Examples:
onready var list = get_node("Contents/ItemList")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func push_connection(id):
	list.add_item(String(id))
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func remove_connection(id):
	for i in range(list.get_item_count()):
		if list.get_item_text(i) == String(id):
			list.remove_item(i)
			return

extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var list = get_node("Contents2/ItemList")

func push_map(id):
	list.add_item(String(id))
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func remove_map(id):
	for i in range(list.get_item_count()):
		if list.get_item_text(i) == String(id):
			list.remove_item(i)
			return

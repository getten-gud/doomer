extends Node


# aStar navigation
var navigation

var map_data = {}

var grid = preload("res://scripts/grid_tools.gd")

var segments = []

var players = {}
var enemies = {}

var tiles = {}
# Called when the node enters the scene tree for the first time.
func init(map_name):
	load_world(map_name)
		
	navigation = AStar.new()
	pass # Replace with function body.

func generate_segment(data):
	var segment = {
		"node": Spatial.instance(),
		"tiles":[],
		"navigation":[]
	}
	
	var offset = data.node.header.global - data.node.header.local
	
	for square in data.squares:
		var xLength = square.maxX - square.minX
		var yLength = square.maxY - square.minY
		
		var connections = []
		
		var points = navigation.get_point_count()
		
		for i in range(xLength * yLength):
			var x = offset.x + (i % xLength)
			var y = offset.y + floor(i / xLength)
			var tile = i+points
			var tile_coord = Vector2(x,y)
			tiles[tile_coord] = tile
			navigation.add_point(i + points, grid.grid_to_world_center((tile_coord)))
			
			## If not on the west edge
			if not i%xLength == 0:
				connections.push([i+points, i + points - 1])
			## If not on the east edge
			elif not i%xLength == xLength - 1:
				connections.push([i+points, i+points + 1])
			## If not the south edge
			if not floor(i/xLength) == 0:
				connections.push([i+points, i+points-xLength])				
			## If not the north edge
			if not floor(i/xLength) == yLength - 1:
				connections.push([i+points, i+points + xLength])
				
		for connection in connections:
			navigation.connect_points(connection[0], connection[1])
			
			
	segment.node.set_name(segments.size())
	segments.push(segment)
	pass
	
remote func add_player(player):
	for playerI in players:
		rpc_id(players[playerI], "add_player", player.data)
	players[player.data.id] = player
	
remote func remove_player(player):
	var id = player.data.id
	players.erase(player.data.id)
	for playerI in players:
		rpc_id(players[playerI], "remove_player", id)

remote func request_spawn():
	var id = get_tree().get_rpc_sender_id()
	var player = players[id]
	
	player.data.position = Vector3(125, 225, 125)
	player.map.get_node("players").add_child(player)
	
	for person in player.map.players:
		rpc_id(player.map.players[person].data.id, "spawn_player", player.data.id, player.data.position)

remote func map_ready():
	var id = get_tree().get_rpc_sender_id()
	rpc_id(id, "add_player", players[id].data)

remote func send_game_data():
	var id = get_tree().get_rpc_sender_id()
	var sending_player = players[id]
	
	var otherPlayerData = []
	for player in players.players:
		if(player != id):
			otherPlayerData.push_back(player.data)
		
	var enemyData = []
	for enemy in enemies:
		enemyData.push_back(enemy.data)
	
	rpc_id(id, "setup_game", otherPlayerData, enemyData)

func save_world():
	var save_game = File.new()
	save_game.open("user://" + map_data.header.map_name + ".world", File.WRITE)
	
	var save_data = map_data
		# Store the save dictionary as a new line in the save file.
	save_game.store_line(to_json(save_data))
	save_game.close()
	
	# Note: This can be called from anywhere inside the tree. This function
# is path independent.
func load_world(worldname):
	var save_game = File.new()
	if not save_game.file_exists("user://" + worldname + ".world"):
		map_data = {
		"header":{
			"map_name":worldname,
		},
		"segments":[]
		}
		save_world()
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://" + worldname + ".world", File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		map_data = parse_json(save_game.get_line())


	save_game.close()

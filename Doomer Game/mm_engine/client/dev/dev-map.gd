extends "res://client/map.gd"

var local_tiles = {}

remote func add_player(player):
	var selfPeerID = get_tree().get_network_unique_id()	
	var player_instance
	if player.id == selfPeerID:
		emit_signal("ready_to_play")
		player_instance = preload("res://client/dev/admin_player.tscn").instance()
		me = player_instance
	else:
		player_instance = preload("res://client/other_player.tscn").instance()
	player_instance.init(player)
	player_instance.set_name(String(player.id))
	players[player.id] = player_instance

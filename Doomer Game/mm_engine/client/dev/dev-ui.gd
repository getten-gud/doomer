extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var tile_window = get_node("tile panel")
onready var console_input = get_node("console panel/contents/LineEdit")
onready var console_output = get_node("console panel/contents/RichTextLabel")

onready var maps_list = get_node("map panel/rows/sections2/maps_list/ItemList")

signal console_command(command)

# Called when the node enters the scene tree for the first time.
func _ready():
	var block_refs = preload("res://client/block_references.gd").new()
	
	var ground_texture_picker = tile_window.get_node("contents/ground style/picker")
	
	for block in block_refs.blocks.tiles.floor:
		ground_texture_picker.add_item(block)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _console_out(msg):
	console_output.append_bbcode("\n"+msg)

func _on_map_window_toggled(button_pressed):
	get_node("map panel").visible = button_pressed

func _on_builder_window_toggled(button_pressed):
	get_node("build panel").visible = button_pressed

func _on_tile_window_toggled(button_pressed):
	get_node("tile panel").visible = button_pressed

func _on_term_toggled(button_pressed):
	get_node("console panel").visible = button_pressed

func _on_console_command(new_text):
	emit_signal("console_command", new_text)
	console_input.clear()

func _on_new_map_button():
	var map_name = get_node("map panel/rows/sections2/maps_list/MapsFooter/input")
	get_tree().get_root().get_node("Game").rpc_id(1, "request_new_map", map_name.text)
	map_name.text = ""

remote func map_added(map):
	add_map(map)

remote func map_removed(map):
	remove_map(map)

func add_map(map):
	maps_list.add_item(map)
	
func remove_map(map):
	for i in range(maps_list.get_item_count()):
		if maps_list.get_item_text(i) == map:
			maps_list.remove_item(i)

func _on_map_selected(index):
	get_node("map panel/rows/sections2/maps_list/MapsFooter/Button2").disabled = false
	rpc_id(1, "get_map_details", maps_list.get_item_text(index))

func _on_section_selected(index):
	pass # Replace with function body.

func _on_world_spawn_button():
	pass # Replace with function body.

func _on_remove_map_button():
	rpc_id(1, "remove_map", maps_list.get_item_text(maps_list.get_selected_items()[0]))

func _on_map_list_nothing_selected():
	maps_list.unselect_all ( )
	get_node("map panel/rows/sections2/maps_list/MapsFooter/Button2").disabled = true

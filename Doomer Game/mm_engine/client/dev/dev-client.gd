extends "res://client/client.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal console_out(msg)

func _ready():
	._ready()
	connect("connection_success", self, "start_ui")
	var dev_ui = get_node("dev")
	dev_ui.connect("console_command", self, "handle_console")
	connect("console_out", dev_ui, "_console_out")
	
func _player_connected(character, nickname, code):
	._player_connected(character, nickname, code)
	rpc_id(1, "register_as_admin", "secret")
	
func start_ui():
	get_node("dev" ).visible = true

func handle_console(msg):
	if msg == "noclip":
		current_map.me._set_noclip(not current_map.me.noclip)
		emit_signal("console_out", "noclip set to: " + String(current_map.me.noclip))

remote func init_map(map_data):
	get_node("BackDrop").visible = false
	get_node("Doomer logo").visible = false
	get_node("Join Panel").visible = false 	
	
	var map_instance = preload("res://client/dev/dev-map.tscn").instance()
	current_map = map_instance
	map_instance.connect("ready_to_play", self, "hide_pregame_ui")
	map_instance.init(map_data)
	add_child(map_instance)
	map_instance.ready()

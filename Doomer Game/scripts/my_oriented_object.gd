extends KinematicBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

	

func push_orientation():
	rpc_unreliable("update_orientation", rotation)

func push_position(velocity):
	rpc_unreliable("update_position", translation, velocity)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends "res://scripts/oriented_object.gd"

export var front_frame = 0
export var back_frame = 0
export var left_frame = 0
export var right_frame = 0

onready var sprite

func _ready():
	if has_node("Sprite"):
		sprite = get_node("Sprite")

func set_direction(direction):
	.set_direction(direction)
	if sprite != null:
		match last_direction:
			"back":
				sprite.frame = back_frame
			"front":
				sprite.frame = front_frame
			"left":
				sprite.frame = left_frame
			"right":
				sprite.frame = right_frame

extends Node

var config

func _init(path):
	config = ConfigFile.new()
	var err = config.load(path)
	if(err != OK):
		print(err)

func get_key(section, key):
	return config.get_value(section, key)

extends KinematicBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


signal direction_set(new_direction)
var last_direction

var velocity = Vector3(0,0,0)

var server = 0


slave func update_orientation(orientation):
	rotation = orientation
	
slave func update_position(position, velocity):
	translation = position
	self.velocity = velocity
# Called when the node enters the scene tree for the first time.
func _physics_process(delta):
	if velocity != null:
		move_and_slide(velocity)

func _process(delta):
	if(server != 1):
		if(db.focal_point != null):
			var fwd = db.focal_point.transform.basis.z
			var focal_angle = Vector2(fwd.x, fwd.z).angle()			
			
			var object_vector = transform.basis.z
			var object_angle = Vector2(object_vector.x, object_vector.z).angle()
			
			var angledelta = focal_angle - object_angle
			
			if angledelta > -PI/4 and angledelta < PI/4:
				set_direction("back")	
			elif(angledelta >= PI/4 and angledelta < (3*PI)/4):
				set_direction("right")
			elif angledelta <= -PI/4 and angledelta > -(3*PI/4):
				set_direction("left")
			else:
				set_direction("front")
		else:
			db.focal_point = get_tree().get_root().get_node("Game").me	
	
func set_direction(direction):
	if(direction != last_direction):
		emit_signal("direction_set", direction)
		last_direction = direction

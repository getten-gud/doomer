extends "res://scripts/sprite_animator.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var weapon_state
var state_data = {}

signal holstered(success)

func set_state(state, data = null):
	weapon_state = state
	state_data = data

func main_fire():
	pass
	
func alt_fire():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	._process(delta)
	if Input.is_action_just_pressed("fire"):
		main_fire()
	if Input.is_action_just_pressed("alt_fire"):
		alt_fire()


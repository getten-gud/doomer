extends "res://scripts/weapons/weapon.gd"

var animations = {
	"idle":{"name":"idle", "frame_rate":1.0, "loop":false, "frames":[0] },
	"turn_right":{"name":"turn_right", "frame_rate":1.0, "loop":false, "frames":[1]},
	"turn_left":{"name":"turn_left", "frame_rate":1.0, "loop":false, "frames":[2]},
	"reload":{"name":"reload", "frame_rate":2.0, "loop":false, "frames":[8,9,10]},
	"fire":{"name":"fire", "frame_rate":4.0, "loop":false, "frames":[3,4,4,6,6,7,7]},
	"holster":{"name":"holster", "frame_rate":4, "loop":false, "frames":[11,12,13,14]},
	"equip":{"name":"equip", "frame_rate":4, "loop":false, "frames":[14,13,12,11]}	
}

var audio = {
	"load":preload("res://sound/SwitchMetalPanelLock_SFXB.1048.wav")
}

onready var camera = get_node('../')
const RELOAD_TIME = 1.5
const MAGAZINE_SIZE = 1.0
const FIRE_TIME = 0.6

onready var player = get_node('../../')
onready var client = get_tree().get_root().get_node("Game")

onready var trash = get_tree().get_root().get_node("Game/trash")
onready var ejection = get_node("Ejection")
const shell = preload("res://database/items/objects/12gabuck_spent.tscn")


var shot = null

const IDLE_STATE = "idle"
const FIRING_STATE = "firing"
const RELOAD_STATE = "reload"
const HOLSTER_STATE = "holster"
const AMMO_SELECT = "ammo_select"


onready var muzzle_light = get_node("muzzle")
var allowed_ammo = ["12ga_buck", "12ga_slug"]

var magazine = []
var loaded = 1

func initialize(data, player):
	pass
	
func _process(delta):
	._process(delta)
	if Input.is_action_just_pressed("reload"):
		show_ammo_options()
	if Input.is_action_just_released("reload"):
		hide_ammo_options()
	
	if(weapon_state == AMMO_SELECT):
		if Input.is_action_just_pressed("ammo1"):
			pass
		
func show_ammo_options():
	var candidates = []
	for item in player.data.inventory:
		var ammo_find = allowed_ammo.find(item.id)
		if ammo_find != -1:
			if candidates.find(item.id) == -1 :
				candidates.push_back(item.id)
	for candidate in candidates:
		var icon = TextureRect.new()
		icon.texture = db.items.get_item_icon(candidate)
		icon.expand = true
		icon.size_flags_horizontal = 3
		icon.size_flags_vertical = 3
		icon.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT
		player.notifier_box.add_child(icon)
	set_state(AMMO_SELECT, {"candidates":candidates})
	
func hide_ammo_options():
	for _i in player.notifier_box.get_children():
		_i.queue_free()	
	set_state(IDLE_STATE)

func select_ammo_option(number):
	pass

func _ready():
	animations.fire.frame_rate = 1.0/(FIRE_TIME/animations.fire.frames.size())
	animations.reload.frame_rate = 1.0/(RELOAD_TIME/animations.reload.frames.size())
	
	register_animation_trigger("fire", 1, funcref(self, "check_weapon" ))
	register_animation_trigger("reload", 1, funcref(self, "check_weapon" ))
	register_animation_trigger("reload", 1, funcref(self, "eject_shell" ))
	
	register_animation_trigger("holster", 1, funcref(self, "finish_holster"))
	register_animation_trigger("equip", 1, funcref(self, "finish_equip"))
		
	register_frame_trigger(3, funcref(self, "light_on"))
	register_frame_trigger(6, funcref(self, "light_off"))
	
	play_animation(animations.idle)

func light_on():
	muzzle_light.visible = true

func light_off():
	muzzle_light.visible = false

	
func holster():
	if(weapon_state != AMMO_SELECT):
		set_state(HOLSTER_STATE)
		play_animation(animations.holster)
	else:
		emit_signal("holstered", false)

func finish_holster():
	emit_signal("holstered", true)

func check_weapon():
	if loaded == 0:
		set_state(RELOAD_STATE)
		play_animation(animations.reload)
		loaded += 1
	else:
		set_state(IDLE_STATE)
		play_animation(animations.idle)

func _physics_process(delta):
	var space_state = get_world().direct_space_state
	# use global coordinates, not local to node
	var result = space_state.intersect_ray(Vector3(0, 0, 0), Vector3(0,0,0))	
	pass

func main_fire():
	if(player.is_ready_to_fire() and loaded > 0 and weapon_state == IDLE_STATE and magazine.size() > 0):
		var projectile = magazine.pop_front()
		play_animation(animations.fire)
		set_state(FIRING_STATE)
		for other_player in client.players_by_id:
			player.rpc_id(other_player, "fire")
		loaded -= 1
		camera
		var from = camera.project_ray_origin(OS.get_window_size()/2)
		var to = from + camera.project_ray_normal(OS.get_window_size()/2) * 5

func equip():
	play_animation(animations.equip)
	frame = 14
	visible = true

func finish_equip():
	set_state(IDLE_STATE)
	play_animation(animations.idle)

func eject_shell():
	var new_shell = shell.instance()
	var ejector_location = ejection.get_global_transform()
	new_shell.transform.origin = ejector_location.origin
	new_shell.transform.basis = ejector_location.basis
	trash.add_child(new_shell)
	new_shell.linear_velocity = Vector3(0,-9,0)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

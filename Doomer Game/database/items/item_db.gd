extends Node

var items = {
	"pumpshot12":{ "item_type":"primary", "label":"Pump 12ga Shotgun", "size":2, "object":preload("res://database/items/objects/pumpshot.tscn"), "viewmodel":preload("res://database/items/viewmodels/Shotgun.tscn"), "icon":preload("res://database/items/icons/pumpshot12.tres") },
	"slugbox12_large":{ "item_type":"inventory", "label":"Large Box of 12ga Slugs", "size":1, "object":preload("res://database/items/objects/slugbox12_large.tscn") },
	"buckbox12_large":{ "item_type":"inventory", "label":"Large box of 12ga Buckshot", "size":1, "object":preload("res://database/items/objects/buckbox12_large.tscn"), "icon":preload("res://database/items/icons/buckbox12_large.tres") },
	"hands_white":{"item_type":"hands", "label":"White man's hands", "size":0, "viewmodel":preload("res://database/items/viewmodels/Hands.tscn")},
	
	"12ga_buck":{"item_type":"inventory", "label":"12ga Buckhot Shell", "size":0.1, "object":preload("res://database/items/objects/12gabuck.tscn"), "icon":preload("res://database/items/icons/12ga_buck.tres")},
	
	"12ga_buck_spent":{"item_type":"inventory", "label":"Spent 12ga Buckshot Shell", "size":0.1, "object":preload("res://database/items/objects/12gabuck_spent.tscn"), "icon":preload("res://database/items/icons/12ga_buck_spent.tres")}
}

func get_item(item):
	var item_data = items.get(item)
	if(item_data == null):
		##error here
		pass
	else:
		return item_data
		
func get_item_icon(item):
	var item_data = items.get(item)
	if(item_data == null):
		##error here
		pass
	else:
		return item_data.get("icon")

func get_item_details(item):
	var item_data = get_item(item)
	
	if item_data != null:
		return {
			"label":item_data.label,
			"item_type":item_data.item_type,
			"size":item_data.size,
			"icon":item_data.get("icon")
		}

func get_world_object(item):
	var item_data = get_item(item)
	
	if item_data != null:
		if item_data.object != null:
			return item_data.object.instance() 
	
func get_view_model(item):
	var item_data = get_item(item)
	
	if item_data != null:
		var viewmodel = item_data.get("viewmodel")
		if viewmodel != null:
			return item_data.viewmodel.instance() 

extends "res://scripts/static_oriented.gd"

export var grants_item = ""
export var root = ""
var interactable = true

signal pressed

func _ready():
	._ready()
	connect("pressed", self, "grant")
	
func grant(player):
	player.grant_item(grants_item)
	
	if root == "":
		queue_free()
	else:
		get_node(root).queue_free()

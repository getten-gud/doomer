extends "res://scripts/weapons/weapon.gd"

var animations = {
	"idle":{"name":"idle", "frame_rate":1.0, "loop":false, "frames":[13] },
	"equip":{"name":"equip", "frame_rate":2.0, "loop":false, "frames":[8,9] },	
	"holster":{"name":"holster", "frame_rate":2.0, "loop":false, "frames":[9,8] },	
	"ready":{"name":"ready", "frame_rate":1.0, "loop":false, "frames":[14] },
	"click":{"name":"click", "frame_rate":1.0, "loop":false, "frames":[15] },	
}

var agent

onready var ray = get_node('./cast') 

const IDLE_STATE = "idle"
const HOLSTER_STATE = "holster"
const READY_STATE = "ready"

var initial_position
var pos_offset = Vector3(0,0,0)

func _process(delta):
		if ray.is_colliding() && weapon_state == IDLE_STATE:
			var collider = ray.get_collider()
			if(collider.get("interactable") != null and collider.interactable == true):
				play_animation(animations.ready)
				set_state(READY_STATE)
		elif !ray.is_colliding() and weapon_state == READY_STATE:
			play_animation(animations.idle)
			set_state(IDLE_STATE)

func init(player):
	agent = player
	
	initial_position = translation

func equip():
	play_animation(animations.equip)
	visible = true
	
func finish_equip():
	play_animation(animations.idle)
	set_state(IDLE_STATE)

func holster():
	play_animation(animations.holster)
	set_state(HOLSTER_STATE)

func finish_holster():
	emit_signal("holstered", true)

func _input(event):
	if event is InputEventMouseMotion and visible:
		var x = event.relative.x * 0.0001
		var y = event.relative.y * -0.0001
		pos_offset.x = clamp(pos_offset.x + x, -0.05, 0.05)
		pos_offset.y = clamp(pos_offset.y + y, -0.0474, 0.03)
		translation = initial_position + pos_offset


func main_fire():
	if(weapon_state == READY_STATE):
		play_animation(animations.click)
		ray.get_collider().emit_signal("pressed", agent)
		
func _ready():
	register_animation_trigger("holster", 1, funcref(self, "finish_holster"))
	register_animation_trigger("equip", 1, funcref(self, "finish_equip"))
		
	
	play_animation(animations.idle)

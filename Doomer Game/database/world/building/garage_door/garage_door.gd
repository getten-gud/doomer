extends Spatial

signal trigger

onready var anim_player = get_node("garage_door/AnimationPlayer")
onready var collider = get_node("CollisionShape")

var open = false

func _ready():
	anim_player.play("Close")
	connect("trigger", self, "toggle")
	
func toggle(agent):
	open = !open
	if open:
		anim_player.play("Open")
		collider.disabled = true
	else:
		anim_player.play("Close")
		collider.disabled = false
		

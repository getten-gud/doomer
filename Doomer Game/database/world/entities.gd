extends Node

var entities =  {
	
	## Nature
	"forest":{
		"forest01":{ "label":"Two Tree Forest 1", "object":preload("res://database/world/tree/forest_01.tscn") },
		"forest02":{ "label":"Two Tree Forest 2", "object":preload("res://database/world/tree/forest_02.tscn") },
	},
	"tree":{
		"tree01":{ "label":"Dead Tree", "object":preload("res://database/world/tree/tree1.tscn") },
		"tree02":{ "label":"Floofy Tree", "object":preload("res://database/world/tree/tree2.tscn") },
	},
	## Buildings
	"warehouse":{
		"brick":{ "label":"Brick Warehouse", "object":preload("res://database/world/building/brick_warehouse.tscn") },
		
	}

}


func get_entity(category, title):
	var entity_group = entities.get(category, {})
	
	var entity = entity_group.get(title)
	
	if entity == null:
		entity = entities[category].get(entities[category].keys()[ randi()%entities[category].size()])
	return entity

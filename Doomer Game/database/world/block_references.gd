extends Node

const DEFAULT_MATERIALS = {
		"floors":{"width":160.0 , "height":160.0 , "x_tiles":5.0 , "y_tiles":5.0 },
		"ceiling":{"width":160.0, "height":160.0 ,  "x_tiles":5.0, "y_tiles":6.0 },
		"wall":{"width":160.0, "height":160.0, "x_tiles":5.0, "y_tiles":6.0}
	}

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const blocks = {
	"tiles":{
		"floors":{
			"gravel":{},
			"grass":{},
			"forest":{},
			"magma":{},
			"volcanic_rock":{},
			"dirt":{},
			"water":{},
			"polluted_water":{},
			"road":{},
			"road_lined_corner":{},
			"road_lined":{},
			"cobblestone":{},
			"cobblestone_path":{}
			
		},
		"wall":{
			"brick":{},
			"blue_cement":{},
			"garage_door":{},
			"rusted_fence":{}
		}
	}
}

func get_wall(wall_name, vert):
	return blocks.tiles.wall.get(wall_name, blocks.tiles.wall.brick)

func get_floor(floor_name, vert):
	return blocks.tiles.floors.get(floor_name, blocks.tiles.floors.gravel).get(vert)

# Called when the node enters the scene tree for the first time.
func _init(materials):
	if materials == null:
		materials = DEFAULT_MATERIALS
	
	var boxSize
	var u = 0.0
	var v = 0.0
	for tile_set in blocks.tiles:
		var mat = materials[tile_set]
		boxSize = {"u":1.0/mat.x_tiles , "v":1.0/mat.y_tiles} 
		var i = 0
		for tile in blocks.tiles[tile_set]:
			var x = i % int(mat.x_tiles)
			var y = floor(i / mat.x_tiles)
			
			u = x * boxSize.u
			v = y * boxSize.v
			
			blocks.tiles[tile_set][tile].TL = Vector2(u, v)
			blocks.tiles[tile_set][tile].TR = Vector2(u+boxSize.u, v)
			blocks.tiles[tile_set][tile].BL = Vector2(u, v + boxSize.v)
			blocks.tiles[tile_set][tile].BR = Vector2(u + boxSize.u, v + boxSize.v)
			i = i + 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

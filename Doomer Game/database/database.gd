extends Node

var focal_point = null

var items = preload("res://database/items/item_db.gd").new()
var tiles = preload("res://database/world/block_references.gd").new(null)	
var entities = preload("res://database/world/entities.gd").new()
var audio = preload("res://database/audio/audio.gd").new()

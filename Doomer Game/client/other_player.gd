extends "res://scripts/oriented_object.gd"


var current_state = "idle"

onready var shot_light = get_node("Shot Light")

signal change_animation_state(state)

func init(data):
	pass

remote func fire():
	change_state("shoot")
	shot_light.visible = true
	yield(get_tree().create_timer(0.2), "timeout")
	shot_light.visible = false
	change_state("idle")

# Called when the node enters the scene tree for the first time.
#func _ready():

func change_state(state):
	current_state = state
	emit_signal("change_animation_state", state)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	._process(delta)
	if(current_state != "walk" and current_state != "shoot" and velocity.length() > 0 ):
		change_state("walk")
	elif(current_state != "idle" and current_state != "shoot" and velocity.length() < 0.1):
		change_state("idle")

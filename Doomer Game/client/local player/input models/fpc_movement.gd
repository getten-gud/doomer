extends Node

var rotationSpeed = 0.005
var walk_speed = 20

func input(event, state):
	var data = {"rotation":Vector3(0,0,0), "cam_rotation":Vector3(0,0,0), "input_vector":Vector3(0,0,0), "camera_rotation":Vector3(0,0,0)} 	
	if event is InputEventMouseMotion:
		if state.vertical_aim == true:
			data.rotation += Vector3(0,event.relative.x * -rotationSpeed/15,0)
			data.camera_rotation = Vector3( clamp( state.camera_rotation.x + event.relative.y * -rotationSpeed/25, -1.039, 1.06 ),state.camera_rotation.y, state.camera_rotation.z)
		else:
			data.rotation += Vector3(0,event.relative.x * -rotationSpeed,0)
			
				
func process(delta, state):
	var data = {"rotation":Vector3(0,0,0), "cam_rotation":Vector3(0,0,0), "input_vector":Vector3(0,0,0), "camera_rotation":Vector3(0,0,0)} 
	if state.vertical_aim:
		data.rotation = Vector3(0, (Input.get_action_strength("look_left") - Input.get_action_strength("look_right")) * 0.05,0)
		data.input_vector.z = Input.get_action_strength("look_up") - Input.get_action_strength("look_down") * 0.05
	else:
		data.rotation += Vector3(0, (Input.get_action_strength("look_left") - Input.get_action_strength("look_right")) * 0.05,0)
		data.cam_rotation = Vector3( clamp( state.camera_rotation.x + (Input.get_action_strength("look_up") - Input.get_action_strength("look_down")) * 0.005, -1.039, 1.06 ), state.camera_rotation.y, state.camera_rotation.z)

	if Input.is_action_just_pressed("inventory"):
		state.player.equipment.open_panel()
	if Input.is_action_just_pressed("drop_weapon"):
		state.player.equipment.drop(state.current_slot)
	if Input.is_action_pressed("up") and data.input_vector.z != -1:
		state.input_vector.z = -1
	if Input.is_action_pressed("down") and data.input_vector.z != 1:
		data.input_vector.z = 1
	if not Input.is_action_pressed("up") and not Input.is_action_pressed("down") and data.input_vector.z != 0:
		data.input_vector.z = 0
	if Input.is_action_pressed("left") and data.input_vector.x != -1:
		data.input_vector.x = -1
	if not Input.is_action_pressed("left") and not Input.is_action_pressed("right") and data.input_vector.x != 0:
		data.input_vector.x = 0
	if Input.is_action_pressed("right") and data.input_vector.x != 1:
		data.input_vector.x = 1
	if Input.is_action_pressed("jump") and state.on_floor == true:
		data.input_vector.y = 5
		data.gravitational_momentum = -9.8
	if Input.is_action_just_pressed("hand"):
		state.player.equipment.change_weapons("hands")
	if Input.is_action_just_pressed("primary1"):
		state.player.equipment.change_weapons("primary1")
	if Input.is_action_just_pressed("unlock"):
		if(state.mouse_locked):
			data.mouse_locked = false
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)			
		else:
			data.mouse_locked = true
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	return data

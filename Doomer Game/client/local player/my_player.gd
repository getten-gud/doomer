extends "res://scripts/my_oriented_object.gd"

var grid = preload("res://scripts/grid_tools.gd")

# Declare member variables here. Examples:
var totalDelta = 0


var velocity_override = false

var map_tile = null

var camera

var map

var gravitational_momentum = 0

var character_state = {
	"vertical_aim":false,
	"runmode":1,
	"in_hand":null,
	"equipment":preload("res://client/local player/equipment.gd").new(),
	"input_vector":{'x':0.0, 'y':0.0, 'z':0.0},
	"aim_vector":{"x":0.0, "y":0.0},
}

var locked = false



var hands
var shotgun

var character_panel
var notifier_box

var input_model = preload("res://client/local player/input models/player_movement.gd").new()



# Called when the node enters the scene tree for the first time.
remote func init(data, map):
	camera = get_node("Camera")
	
	character_state.equipment.init(data)
	
	

	camera.add_child(hands)
	
	
	set_name(String(data.id))
	
	character_panel = preload("res://client/container/inventory_panel.tscn").instance()
	character_panel.initialize(data, self)
	character_panel.connect("drop", self, "_drop_item")
	
	add_child(character_panel)
	character_panel.visible = false
	
	self.map = map
	
	notifier_box = get_node("notifcier_box")
	


func toggle_character_panel():
	character_panel.visible = !character_panel.visible
	if character_panel.visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		
		
func _input(event):
	input_model.input(event, character_state)
	
	
func update_aim_vector():
	pass

func change_weapons(new_slot):
	camera.rotation.x = 0
	if character_state.in_hand != null:
		holster()
		var holstered = yield(current_viewmodel, "holstered")
		if(holstered):
			current_viewmodel.visible = false
			if viewmodels[new_slot] != null:	
				current_viewmodel = viewmodels[new_slot]
				current_slot = new_slot
				current_viewmodel.equip()
	else:
		current_viewmodel = viewmodels[new_slot]
		current_slot = new_slot
		current_viewmodel.equip()

func holster():
	character_state.equipment


	
func _drop_item(index):
	var item = data.inventory[index]
	map.spawn_item(item.id, translation)
	character_panel.remove_from_list(index)
	data.inventory.remove(index)
	
func drop_equipment(slot):
	var equipment = viewmodels[slot]
	equipment.queue_free()
	
	var item = data.equipment[slot]
	map.spawn_item(item.id, translation)
	character_panel.remove_equipment(slot)
	data.equipment.erase(slot)
	
	if slot == current_slot:
		current_slot = null
		current_viewmodel = null

func grant_item(item = null, data=null):
	var item_object = data
	if(data == null):
		item_object = {"id":item}
	character_panel.add_inventory_item(item_object)


func is_ready_to_fire():
	if character_panel.visible:
		return false
	else:
		return true
		
func change_tile(coord):
	map_tile = coord

func _process(delta):

	var _actual_velocity
	if not velocity_override:
		if not is_on_floor():
			gravitational_momentum += 9.8*9.8  * delta
		elif gravitational_momentum != 0:
			gravitational_momentum = 0
		_actual_velocity = move_and_slide(transform.basis.xform(Vector3(inputVector.x, 0, inputVector.z).normalized() )* walk_speed + Vector3(0,- gravitational_momentum,0) + Vector3(0,inputVector.y * 10,0) , Vector3(0,1,0), true)
	
		if(inputVector.y > 0):
			inputVector.y -= 0.25
		
	var coord = grid.world_to_grid(translation)
	if(map_tile != coord):
		change_tile(coord)
	
	if(totalDelta > 0.1):
		push_position(_actual_velocity)
		push_orientation()
		totalDelta=0
	
	totalDelta += delta


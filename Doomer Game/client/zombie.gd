extends "res://scripts/oriented_object.gd"

var current_state = "idle"
signal change_animation_state(state)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

remote func attack():
	change_state("attack")
	yield(get_tree().create_timer(0.3), "timeout")
	change_state("idle")

func change_state(state):
	current_state = state
	emit_signal("change_animation_state", state)

func _process(delta):
	._process(delta)
	if(current_state != "walk" and current_state != "attack" and velocity.length() > 0 ):
		change_state("walk")
	elif(current_state != "idle" and current_state != "attack" and velocity.length() < 0.1):
		change_state("idle")

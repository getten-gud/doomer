extends Label


export var phrases = ["redoomption", "redoomening", "undoomerfied", "okay doomer",
"internal", "doomnation", "& Doomerer", "Current Year"]


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var number = randi()%phrases.size()
	text = phrases[number]
	get_node("subtitle").text = phrases[number]
	print(String(number))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

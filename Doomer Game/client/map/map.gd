extends Node

var BlockMap
var BuildMesh
var players = {}

const CHUNK_SIZE = 12.0

signal ready_to_play

var map_data

var me

var chunks = {}

var containers

# Called when the node enters the scene tree for the first time.
func init():
	## Create chunks for the map
	containers = {
		"players":get_node("players"),
		"enemies":get_node("zombies"),
		"items":get_node("items"),
		"chunks":get_node("chunks")
	}
	
	BuildMesh = preload("res://client/draw_mesh.gd").new(self)
		
	#var map_data = from_image("res://img/world-map.png", "res://img/world-height.png", "res://img/world-buildings.png")
	
func initialize_map_data(map_data):
	set_name(map_data.header.name)
	var chunks_x = ceil(map_data.header.resolution.x/CHUNK_SIZE)
	var chunks_y = ceil(map_data.header.resolution.y/CHUNK_SIZE)

	for chunk in range(chunks_x * chunks_y):
		var new_chunk = preload("res://client/map/chunk.tscn").instance()
		containers.chunks.add_child(new_chunk)
		
	var tile_data = BuildMesh.generate_segment(map_data)

func load_complete():
	if get_tree().get_network_unique_id() != 1:
		rpc_id(1, "map_ready")
	else:
		add_player(get_tree().get_root().get_node("Game").selected_character)
		
func from_image(image, height, props):
	var ImgMap = preload("res://client/map_from_image.gd")
	var map_data = ImgMap.generate_map_from_image(image, height, props)
	return map_data
	
func add_player(player):
	var selfPeerID = get_tree().get_network_unique_id()	
	var player_instance
	if player.id == selfPeerID:
		emit_signal("ready_to_play")
		player_instance = preload("res://client/local player/my_player.tscn").instance()
		me = player_instance
		db.focal_point = me
	else:
		player_instance = preload("res://client/other_player.tscn").instance()
	player_instance.init(player, self)
	player_instance.set_name(String(player.id))
	players[player.id] = player_instance

remote func remove_player(id):
	var player = players.get(id)
	if player != null:
		player.queue_free()
		players.erase(id)

func spawn_player(id, position):
	var player = players.get(id)
	
	
	player.translation = position
	containers.players.add_child(player)
	player.set_network_master(id)
	
remote func despawn_player(id):
	remove_child(players[id])

func spawn_item(item, coordinates):
	var item_object = db.items.get_world_object(item)
	item_object.translation = coordinates
	containers.items.add_child(item_object)
	
func spawn_zombie(data):
	var zombie = preload("res://client/zombie.tscn").instance()
	zombie.init(data)
	containers.enemies.add_child(zombie)

func save():
	pass

func draw():
	for chunk in containers.chunks.get_children():
		chunk.draw()

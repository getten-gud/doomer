extends Node


var floor_collider
var floors

var init = false

var draw = false

var entities = {}

# PoolVectorXXArrays for mesh construction.
var verts = PoolVector3Array()
var uvs = PoolVector2Array()
var normals = PoolVector3Array()
var indices = PoolIntArray()

func initialize():
	floors = get_node("Ground Mesh")
	floor_collider = get_node("Floor Collision/CollisionShape")
	init = true
	
func register_entity(coords, data):
	var entity_model = db.entities.get_entity(data.get("category"), data.get("title")).object.instance()
	entity_model.translation = coords
	
	var offset = data.get("offset", {})
	
	entity_model.translation.x += offset.get("x",0)
	entity_model.translation.y += offset.get("y",0)
	entity_model.translation.z += offset.get("z",0)
	
	add_child(entity_model)

func add_tile(coords, tile_data):
	if !init:
		initialize()

	for vert in tile_data.corners:
		tile_data[vert] = verts.size()
		verts.append(tile_data.tile_verts[vert])
		normals.append(tile_data.tile_normals[vert])
		uvs.append(db.tiles.get_floor(tile_data.data.materials.floors, vert))
	
	indices.append(tile_data.TL)
	indices.append(tile_data.BL)
	indices.append(tile_data.BR)
	
	indices.append(tile_data.TL)
	indices.append(tile_data.BR)
	indices.append(tile_data.TR)
	
	for piece in tile_data.data.pieces:
		register_entity(coords, piece)

func draw():
	if(!init):
		initialize()
	if(verts.size() == 0):
		return
	var arr = []
	arr.resize(Mesh.ARRAY_MAX)	
		

	# Assign arrays to mesh array.
	arr[Mesh.ARRAY_VERTEX] = verts
	arr[Mesh.ARRAY_TEX_UV] = uvs
	arr[Mesh.ARRAY_NORMAL] = normals
	arr[Mesh.ARRAY_INDEX] = indices
	
	# Create mesh surface from mesh array.
	floors.mesh = ArrayMesh.new()
	floors.mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arr) # No blendshapes or compression used.	
	floor_collider.shape = floors.mesh.create_trimesh_shape()

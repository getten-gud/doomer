extends "res://mm_engine/client/client.gd"


signal connection_failed
signal connection_success
signal connection_problem
signal error

var config

var current_map
var selected_character = {
			"id":1,
			"nickname":null,
			"character":null,
			"hands":"hands_white",
			"hp":100,
			"equipment":{"primary1":{"id":"pumpshot12"}},
			"inventory":[{"id":"buckbox12_large"}, {"id":"12ga_buck"}],
			"map":null
		}

var players_connected = []
var players_by_id = {}
var players_by_nickname = {}
var admins_connected = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	config = preload("res://scripts/config_loader.gd").new("res://cfg/client.tres")
	#Map.fill_space(-5,-5,5,5)
	#spawn_my_player(Vector3(-25,0,-25))
	
remote func bad_nickname(msg):
	get_node("Join Panel").visible = false
	client.disconnect_from_host()
	emit_signal("error", msg)
	emit_signal("connection_problem")

func websocket_join_server(character, nickname, code = null):
	client = WebSocketClient.new();
	
	var url = "ws://" + "127.0.0.1" + ":" + "8080" # You use "ws://" at the beginning of the address for WebSocket connections

	client.connect("connection_failed", self, "connection_failed")
	client.connect("connection_succeeded", self, "_player_connected", [character, nickname, code])
	
	var error = client.connect_to_url(url, PoolStringArray(), true);
	
	get_tree().set_network_peer(client)

func join_server(ip, port, character, nickname, code = null):
	var peer = NetworkedMultiplayerENet.new()
	
	peer.connect("connection_failed", self, "connection_failed")
	peer.connect("connection_succeeded", self, "_player_connected", [character, nickname, code])
	
	peer.create_client(ip, port)
	get_tree().set_network_peer(peer)
	
func create_server(SERVER_PORT):
	var peer = NetworkedMultiplayerENet.new()
	var _error = get_tree().connect("network_peer_connected", self, "_other_player_connected")
	_error = get_tree().connect("network_peer_disconnected", self, "_other_player_disconnected")
	peer.create_server(SERVER_PORT, 32)
	get_tree().network_peer = peer
	
func test_mode():
	var map_from_image = preload("res://client/map_from_image.gd").new()
	var map = map_from_image.generate_map_from_image( "res://img/world-map.png", "res://img/world-height.png", "res://img/world-buildings.png" )
	create_server(8081)
	init_map(map)
	
func _process(delta):
	if client != null:
		client.poll()  

func _player_connected(character, nickname, code):
	rpc_id(1,"request_map", character, nickname, code)
	emit_signal("connection_success")

func _other_player_connected(id):
	var player_instance = preload("res://client/other_player.tscn").instance()
	player_instance.init(id)
	players_by_id[id] = player_instance
	players_connected.push_back(player_instance)
	emit_signal("add_client", id)
	emit_signal("output", "global", "Player by ID [b]" + String(id) + "[/b] connected.")
	player_instance.set_network_master(id)
	
func _other_player_disconnected(id):
	var player = players_by_id[id]	
	players_by_id[id].disconnect_from_game()
	emit_signal("remove_client", id)
	
	remove_player(id)
		
	emit_signal("output", "global", "Player by ID [b]" + String(id) + "[/b] disconnected.")	

func connection_failed():
	emit_signal("connection_failed")
	
func hide_pregame_ui():	
	get_node("ui").visible = false
	get_node("Join Panel/content/button").connect('pressed', self, "play_game_button")
	get_node("Join Panel").visible = true
	
func play_game_button():
	var id = get_tree().get_network_unique_id()
	
	if id != 1:
		current_map.rpc_id(1, "request_spawn")
	else:
		current_map.spawn_player(1, Vector3(125, 225, 125))
	get_node("Join Panel").visible = false

func init_map(map_data):
	show_main_menu(false)
	
	var map_instance = preload("res://client/map/map.tscn").instance()
	current_map = map_instance
	map_instance.connect("ready_to_play", self, "hide_pregame_ui")
	add_child(map_instance)
	map_instance.init()
	map_instance.initialize_map_data(map_data)
	map_instance.load_complete()
	
func show_main_menu(show):
	get_node("BackDrop").visible = show
	get_node("Doomer logo").visible = show
	get_node("Join Panel").visible = show 	

func remove_player(id):
	var player = players_by_id[id]
	if players_by_nickname.get(player.data.nickname):
		admins_connected.erase(player)
		var map = player.map
		if map != null:
			map.remove_player(player)
		emit_signal("remove_player", player.data.nickname)
		players_by_nickname.erase(player.data.nickname)
	players_by_id[id].queue_free()
	players_by_id.erase(id)

remote func request_map(character, nickname, code):
	var id = get_tree().get_rpc_sender_id()
	
	if players_by_nickname.get(nickname) != null:
		rpc_id(id, "bad_nickname", "Nickname is already taken.")
		return
	
	var player = players_by_id[id]
	player.set_name(String(id))
	player.set_details(character, nickname, code)	
	#assign_player_to_map(player)
	players_by_nickname[nickname] = player
	
	#if map != null:
	#	rpc_id(id, "init_map", map.map_data)

func assign_player_to_map(player, map):
	player.map = map
	
	map.add_player(player)
	
	emit_signal("add_player", player.data.nickname)
	emit_signal("output", "global", "Player [b]" + player.data.nickname + "[/b] has been added to map [b]" + map.name + "[/b]")
	#choice_map.node.get_node("players").add_child(player)

extends Node


# Declare member variables here. Examples:
var character = "shaun"
var nickname = "ha ha ha"

onready var display = {
	"logo":get_node("../Doomer logo"),
	"subtitle":get_node("../Doomer logo/old-subtitle"),
	"alert":get_node("../Prompt")
}

onready var game = get_node("../")

onready var ins = {

}

# Called when the node enters the scene tree for the first time.
func _ready():
	game.connect("connection_failed", self, "_connection_failed")
	game.connect("connection_problem", self, "_on_connection_problem")
	game.connect("error", self, "_display_error")

# Called every frame. 'delta' is the elapsed time since the previous frame.
# func _process(delta):

func _display_error(error):
	display.alert.dialog_text = error
	display.alert.popup_centered()

func _connection_failed():
	enable_inputs()
	enable_play_button()

func _player_name_changed(new_text):
	nickname = new_text

func _on_Button_pressed():
	game.test_mode()
	disable_inputs()
	disable_play_button()
	
func _on_connection_problem():
	enable_inputs()
	
func disable_inputs():
	pass
	
func enable_inputs():
	pass
	
func disable_play_button():
	pass

func enable_play_button():
	pass
	


extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var tiles = {}

func add_block(scene, x,y, pieces, doors, materials):
	tiles[Vector2(x,y)] = {"pieces":pieces, "doors":doors, "materials":materials, "height":rand_range(0, 4)}

func fill_space(scene, minX, minY, maxX, maxY):
	var xLen = maxX - minX
	var yLen = maxY - minY
	
	var doorLoc = randi() % (xLen-1) + minX + 1
	
	for i in range(xLen * yLen):
		var x = minX + (i%xLen)
		var y = minY + floor(i / xLen)
		var block = []
		var door = []
		
		var materials = {
			"floor":"gravel",
			"wall":"brick",
			"ceiling":"",
			"door":"wood window"
		}
		
		if x == doorLoc and y == maxY -1:
			block.push_back('ndoubledoor')
		elif x == minX and y == minY:
			block.push_back('south')
			block.push_back('east')
		elif x == maxX - 1 and y == minY:
			block.push_back('south')
			block.push_back('west')
		elif x == minX and y == maxY - 1:
			block.push_back('north')
			block.push_back('east')
		elif x == maxX - 1 and y == maxY - 1:
			block.push_back('north')
			block.push_back('west')
		elif x == maxX - 1:
			block.push_back('west')
		elif x == minX:
			block.push_back('east')
		elif y == maxY - 1:
			block.push_back('north')
		elif y == minY:
			block.push_back('south')

		add_block(scene, x, y, block, door, materials)

extends Node

static func generate_map_from_image(image, height, buildings):
	var pallette = {
		"4d7a3f":"grass",
		"38582d":"forest",
		"834a45":"gravel", ## building
		"41658e":"gravel", ## House
		"6b5846":"dirt",
		"96785c":"dirt", ## path
		"00ddff":"water",
		"7250df":"polluted_water",
		"df7733":"magma",
		"3c2626":"volcanic_rock",
		"a1a1a1":"gravel",
		"7a7a7a":"gravel", # rock wall
		"bb8b8b":"cobblestone",
		"a77d7d":"cobblestone_path",
		"2b2b2b":"road", ## Northern road
		"606060":"gravel", # Pavement
		"000000":"road", ## highway
	}
	
	var building_pallette = []
	
	var tile_image = load(image).get_data()
	var height_image = load(height).get_data()
	var building_image = load(buildings).get_data()
	tile_image.lock()
	height_image.lock()
	building_image.lock()
	
	var header = {
		"name":"New Map",
		"resolution": {"x":tile_image.get_width(), "y":tile_image.get_height()}
		
	}
	
	var map = {"header":header, "tiles":{}}
	var building = 0
	
	for y in range(tile_image.get_height()):
		for x in range(tile_image.get_width()):
			var tile_type = pallette[ tile_image.get_pixel(x, y).to_html(false) ]
			var tile_height = pow(100, height_image.get_pixel(x,y).r * 1)
			
				
			var tile = {
				"height":tile_height,
				"materials":{"floors":tile_type},
				"pieces":[],
			}
			
			if tile_type == "forest":
				tile.pieces.push_back({"category":"forest", "title":null})
			
			if building_image.get_pixel(x,y).to_html(false) == "ffffff":
				if(building_pallette.size() > building):
					tile.pieces.push_back({"category":"building", "title":building_pallette[building]})
					building += 1
				else:
					tile.pieces.push_back({"category":"warehouse","title":null})
			
			map.tiles[Vector2(x,y)] = tile
			
	return map

extends Control

var slots 

onready var inventory_list

signal drop(item)

var player

var default_icons = {
	"head":preload("res://client/container/icons/head.tres"),
	"back":preload("res://client/container/icons/back.tres"),
	"chest":preload("res://client/container/icons/chest.tres"),
	"legs":preload("res://client/container/icons/legs.tres"),
	"primary1":preload("res://client/container/icons/primary.tres"),
	"primary2":preload("res://client/container/icons/primary.tres"),
	"secondary1":preload("res://client/container/icons/secondary.tres")
}

var prefabs = {
	"equipment_slot":preload("res://client/container/modules/equipment_slot.tscn")
}

func initialize(data, player):
	
	
	inventory_list = get_node("main/contents/inventory/items")
	slots = { 
		"head":get_node("main/contents/equipment/head"),
		"back":get_node("main/contents/equipment/back"),
		"chest":get_node("main/contents/equipment/chest"),
		"legs":get_node("main/contents/equipment/legs"),
		"primary1":get_node("main/contents/equipment/primary1"),
		"primary2":get_node("main/contents/equipment/primary2"),
		"secondary1":get_node("main/contents/equipment/secondary1")
	}
	
	for slot in slots:
		slots[slot].set_drag_forwarding(self)
	
	inventory_list.set_drag_forwarding(self)
	
	for equip in data.equipment:
		equip_item( equip, data.equipment[equip] )
	for item in data.inventory:
		add_inventory_item(item)
	
	self.player = player

func add_slot(id, rules):
	pass

func equip_item(slot, object):
	var item_data = db.items.get_item_details(object.id)
	if(item_data.get("icon") != null):
		slots[slot].icon = item_data.icon
	
func add_inventory_item(object):
	var item_data = db.items.get_item_details(object.id)
	inventory_list.add_item(item_data.label)
	
func drop_item(index):
	emit_signal("drop", index)

func remove_from_list(index):
	inventory_list.remove_item(index)

func remove_equipment(slot):
	slots[slot].icon = default_icons[slot]

func _on_drop_pressed():
	if inventory_list.is_anything_selected():
		drop_item(inventory_list.get_selected_items()[0])

func can_drop_data_fw(position, data, from_control):
	if(db.items.get_item_details(data.object.id).item_type == from_control.get("item_type") || from_control.item_type == "inventory"):
		return true
	else:
		return false

func drop_data_fw(position, data, from_control):
	player.remove_thing(data.origin.slot, data.origin.number)
	player.place_thing(from_control.slot_name, data.object)
	
func get_drag_data_fw(position, from_control):
	var drag_data = {}
	if from_control.item_type == "inventory":
		if inventory_list.is_anything_selected():
			var select_item = inventory_list.get_selected_items()[0]
			drag_data.object = player.data.inventory[ select_item ]
			drag_data.origin = {"slot":from_control.slot_name, "number":select_item}
	else:
		drag_data.object = player.data.equipment.get(from_control.slot_name)
		drag_data.origin = {"slot":from_control.slot_name, "number":null}
		print(drag_data)
	
	if(drag_data.get("object") == null):
		return null
	
	var icon = TextureRect.new()
	icon.expand = true
	icon.set_size(Vector2(32,32))
	icon.texture = db.items.get_item_icon(drag_data.object.id)
	set_drag_preview( icon )
	return drag_data

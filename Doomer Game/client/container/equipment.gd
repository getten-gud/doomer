extends Node

var slots = {}

func init(data, host):
	pass

func create_slot(id, rules):
	var existing_slot = slots.get(id)
	var slot_item = null
	if existing_slot != null:
		print("WARNING: PREVIOUS SLOT RULES OVERWRITTEN")
		slot_item = existing_slot.get("item")
		
	slots[id] = { "rules":rules, "item":slot_item }
	
	

func equip_item(slot, equipment):
	var viewmodel = db.items.get_view_model(equipment.id)
	viewmodels[slot] = viewmodel
	viewmodel.visible = false
	camera.add_child(viewmodel)
	data.equipment[slot] = equipment
	
	if(character_panel != null):
		print(slot, equipment)
		character_panel.equip_item(slot, equipment)
		
func remove_equipment(slot):
	var equipment = viewmodels[slot]
	equipment.queue_free()
	viewmodels.slot = null
	character_panel.remove_equipment(slot)
	data.equipment.erase(slot)
	if slot == current_slot:
		current_slot = null
		current_viewmodel = null
		
func remove_thing(slot, number):
	if slot == "main_inventory":
		inventory.remove_item(number)
	elif data.equipment.get(slot) != null:
		remove_equipment(slot)
	
func place_thing(slot, object):
	if slot == "main_inventory":
		grant_item(null, object)
	else:
		equip_item(slot, object)

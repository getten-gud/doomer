extends Node

var inventory = []

var inventory_by_item = {}

var slots = {
		"hands":{}
};

func init(data):
	var hands = data.get("hands")
	if hands == null:
		hands = preload("res://database/items/viewmodels/Hands.tscn").instance()
	else:
		hands = db.items.get_view_model(hands)
	
	hands.init(self)
	hands.visible = false
	slots.hands.viewmodel = hands
	
	var character_slots = data.equipment.get("slots")

func remove_item(id):
	var item = inventory_by_item.get(id)
	
	if item == null:
		print("Remove Item: Cannot find item " + str(id))
	inventory.remove(item)

func add_item(item):
	inventory.push_back(item)
	inventory_by_item[item.id] = inventory.size()



func set_slot(slot, item):
	pass
	
func get_viewmodel(item):
	pass

extends "res://client/my_player.gd"


var noclip = false

func init(data):
	.init(data)

func change_tile(coord):
	.change_tile(coord)
	
func _set_noclip(state):
	noclip = state
	velocity_override = state
	get_node("CollisionShape").disabled = state

func _process(delta):
	if(noclip):
		if Input.is_action_pressed("jump") and inputVector.y != 1:
			inputVector.y = 1
		if Input.is_action_pressed("crouch") and inputVector.y != -1:
			inputVector.y = -1
		if not Input.is_action_pressed("jump") and not Input.is_action_pressed("crouch") and inputVector.y != 0:
			inputVector.y = 0
		
		move_and_slide(transform.basis.xform(Vector3(inputVector.x, inputVector.y, inputVector.z).normalized() * walk_speed * 2))
		

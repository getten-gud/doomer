extends "res://scripts/sprite_animator.gd"

const animations = {
	"front":{
		"unarmed":{"name":"unarmed", "frame_rate":1.0, "loop":true, "frames":[0]},
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[1] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[1,2,1,3] },
		"pull_pants":{"name":"pull_pants", "frame_rate":1.0, "loop":false, "frames":[4,5] },
		"poopin":{"name":"poopin", "frame_rate":1.0, "loop":true, "frames":[6] },
		"shoot":{"name":"shoot", "frame_rate":1.0, "loop":false, "frames":[7] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[8,9,10,11] },	
	},
	"back":{
		"unarmed":{"name":"unarmed", "frame_rate":1.0, "loop":true, "frames":[12]},
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[13] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[13,14,13,15] },
		"pull_pants":{"name":"pull_pants", "frame_rate":1.0, "loop":false, "frames":[16,17] },
		"poopin":{"name":"poopin", "frame_rate":1.0, "loop":true, "frames":[18] },
		"shoot":{"name":"shoot", "frame_rate":1.0, "loop":false, "frames":[19] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[20,21,22,23] },	
	},
	"left":{
		"unarmed":{"name":"unarmed", "frame_rate":1.0, "loop":true, "frames":[24]},
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[25] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[25,26,25,27] },
		"pull_pants":{"name":"pull_pants", "frame_rate":1.0, "loop":false, "frames":[28,29] },
		"poopin":{"name":"poopin", "frame_rate":1.0, "loop":true, "frames":[30] },
		"shoot":{"name":"shoot", "frame_rate":1.0, "loop":false, "frames":[31] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[32,33,34,35,36,37] },	
	},
	"right":{
		"unarmed":{"name":"unarmed", "frame_rate":1.0, "loop":true, "frames":[38]},
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[39] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[39,40,39,41] },
		"pull_pants":{"name":"pull_pants", "frame_rate":1.0, "loop":false, "frames":[42,43] },
		"poopin":{"name":"poopin", "frame_rate":1.0, "loop":true, "frames":[44] },
		"shoot":{"name":"shoot", "frame_rate":1.0, "loop":false, "frames":[45] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[46,47,48,49,50,51] },	
	}

}

onready var orienter = get_node("../")

# Called when the node enters the scene tree for the first time.
func _ready():
	play_animation(animations["front"]["idle"])
	orienter.connect("direction_set", self, "change_direction")
	orienter.connect("change_animation_state", self, "change_state")

func change_direction(direction):
	if(current_animation.animation != null):
		switch_animation(animations[direction][current_animation.animation.name])


func change_state(state):
	play_animation(animations[orienter.last_direction][state])
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends "res://scripts/sprite_animator.gd"

const animations = {
	"back":{
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[0] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[0,1,0,2,3] },
		"attack":{"name":"attack", "frame_rate":1.0, "loop":false, "frames":[4,5] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[6,7,8,9] },	
	},
	"front":{
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[10] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[10,11,10,12,13] },
		"attack":{"name":"attack", "frame_rate":1.0, "loop":false, "frames":[14,15] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[16,17,18,19] },	
	},
	"right":{
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[20] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[20,21] },
		"attack":{"name":"attack", "frame_rate":1.0, "loop":false, "frames":[22,23] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[24,25,26,27] },	
	},
	"left":{
		"idle":{"name":"idle", "frame_rate":1.0, "loop":true, "frames":[28] },
		"walk":{"name":"walk", "frame_rate":6.0, "loop":true, "frames":[28,29] },
		"attack":{"name":"attack", "frame_rate":1.0, "loop":false, "frames":[30,31] },
		"death":{"name":"death", "frame_rate":1.0, "loop":true, "frames":[32,33,34,35] },	
	}

}

onready var orienter = get_node("../")

# Called when the node enters the scene tree for the first time.
func _ready():
	play_animation(animations["front"]["idle"])
	orienter.connect("direction_set", self, "change_direction")
	orienter.connect("change_animation_state", self, "change_state")

func change_direction(direction):
	if(current_animation.animation != null):
		switch_animation(animations[direction][current_animation.animation.name])


func change_state(state):
	play_animation(animations[orienter.last_direction][state])
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

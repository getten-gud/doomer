extends VBoxContainer

var manifest_data

var dropdown

func _ready():
	pass
	
func get_profiles():
	var save_game = File.new()
	if not save_game.file_exists("user://characters.save"):
		manifest_data = {
		"characters":[],
		"worlds":[],
		}
		save_manifest()
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://characters.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		manifest_data = parse_json(save_game.get_line())


	save_game.close()

func save_manifest():
	var save_game = File.new()
	save_game.open("user://characters.save", File.WRITE)
	
	var save_data = manifest_data
		# Store the save dictionary as a new line in the save file.
	save_game.store_line(to_json(save_data))
	save_game.close()
	
	# Note: This can be called from anywhere inside the tree. This function
# is path independent.
func load_character(character_name):
	var save_game = File.new()
	var character 
	
	if not save_game.file_exists("user://" + character_name + ".character"):
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://" + character_name + ".character", File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		character = save_game.get_line()

	save_game.close()
	return character


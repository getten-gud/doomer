# Doomer Design

Doomer is a Multiplayer Role playing first person shooter with retro visuals.

# Setting

Doomer is set in a small `mining and manufacturing` town called `Quiet Hill` during the middle years of a Zombie Apocalypse. Quiet Hill- located atop a dormant volcano is an ideal place for producing firearms and munitions and as a result, long before the zombies came along the town was run by three groups: Chemists- specializing in producing propellants and plastics, Smiths- who represent the miners, smelters, and smiths themselves. The last group- is a coalition of tradespeople which was formed by local tailors, traders and anyone else who doesn't fit into one of the first two groups.

*(see: [Quiet Hill](/quiet-hill/).)*

## Key Features

- **Homage:** The game's universe, characters, setting, themes, and plots all revolve around greats in related genres. (*see: [references](/references/).*)
- **Multiplayer Architecture:** Browser based open world, open multiplayer. (*see [multiplayer](/multiplayer/).*)
- **Ammo Based Economy:** Shotgun shells are dollars, the economy is expanded by increasing the production of ammunition (*see:[economy](/economy/*).
- 
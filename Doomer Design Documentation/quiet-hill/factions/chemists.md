# The Chemists

The Chemists were brought in by the miners to blast rock, clearing highways and mines. They found Quiet Hill's nearby volcano appealing as a source of sulphur and created a blighted district of town with a heavy air that reeks of death and ammonia.


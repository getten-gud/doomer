# The Coalition of Tradesfolke

An affiliation originally started by the Tailors of Quiet Hill. Owns the market district of the town, where most trade activity takes place.

The coalition was originally created as a means of standing up to the [miners][miners] and the [chemists](chemists) in Quiet Hill and provided a much needed identity for people living and working in the town who weren't a member of either of the aforementioned groups.

### Reference

A reference to the trade skilling guild based in Freeport in Everquest 2 by the [same name](https://eq2.fandom.com/wiki/Category:The_Coalition_of_Tradesfolke_(Faction)).
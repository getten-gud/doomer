# Quiet Hill

## History

#### What makes Quiet Hill

Quiet Hill is local to very rich iron ore deposits, and because of this it was originally established as a mining town. Roads and mines were blasted into the rock and what is now the town's main drag (Elm St.) was established. Chemists were drawn to working at Quiet Hill because it was built near a volcano and sulphur was plentifully available onsite. The mining front took place largely on one end of Elm, while the chemists occupied the other. Not out of distate for one another, but to be closer to the iron deposits on one side, and the sulphur rich volcano on the other.

#### The Furnace

When the miners set out to build a blast furnace, the size of the workforce plus the Chemists on the other end of town made Quiet Hill an appealing place for traders looking to make a fresh start for themselves, and this in turn brought all sorts of tradespeople in. Quiet Hill got its first general store, an inn, and two large scale housing units. The housing units were placed on opposite ends of Elm street and were designed to house the Mining and new Smithing workers in one house, and the Chemists in the other.

#### A Call to Arms Production

The town began manufacturing firearms and ammunition. This change brought tailors to prominance due to the reliance of cotton to make propellants. Within a few short years of starting manufacture, for every ten tailors, only one was doing something other than preparing cotton for propellant. They needed more space and refused to take up homes with the Alchemists who had polluted their end of town with a permanent smog. The mining district was already overpopulated as it was and tailors were occupying abandoned properties on beaten trails off of Elm street. The Tailors lacked the financial power that the Chemists and Miners have, and so instead of creating a house for Tailors, they instead created the Coalition of Tradesfolke, a house dedicated to anyone left out by the Mining/Chemistry duality.

#### A Voice for the Voiceless

The length of elm street was intersected almost perfectly in half by the highway, originally just a horse trail. Turn one way, and you'll hit the miners, turn the other, and eventually you reach the chemists. The Coalition of Tradesfolke decided to expand the elm-highway intersection into a four way, and created their own house at the end of a new road. The new house ended up being the most extravigant of the three, with some groups of laborers like carpenters having also having made substantial gains with the expansion of Quiet Hill. It also featured places to shop and a wide variety of services, making it the draw of the town.

#### Speaking with the Dead

Chemists had trouble sourcing nitrogen for nitrocellulose production. This made it difficult to get really powerful rounds, the chemists were getting desparate and their attempts at drawing nitrogen from the air had so far yielded nothing. However, one of the chemists who had been living in a cabin nearby had a book bound in flesh and word was getting around about it. Several chemists working on the nitrogen project were dropping it to delve through the pages and attempt to decrypt the books meaning.

A small team of chemists in a shack somewhere off of market street complete the first working nitrogen generator by super-cooling air and extracting the nitrogen in a liquid state. Shortly after they had finished their first successful trial they were attacked and eaten by other chemists. Hours later, more attacks occurred in the market district and house, and finally at the mining district.

#### Quiet Hill Unearthed

Shaun was on the highway and a detour lead him into Quiet Hill. When he reaches Elm street he is distracted by a crashed car on the road, someone steps in front of his car and he crashes into the vehicle on the road. He is attacked by the person he hit immediately on exiting his car and runs away to a bar located near the intersection of Elm, Market, and the Highway. Let's hope that shotgun over the bar is real!

Coach's recent past with the green virus left him effectively homeless and begrudgingly experienced in the area of zombie dismemberment. He could smell them from the highway. He came into town to a scene on the corner of Elm, Market, and the highway and plowed his car through a crowd of zombies straight into two other cars. Before the dust had settled, Coach was gone, en route to the chemistry side of elm street.

## Districts

### The Intersection

Where the Highway meets Elm Street and Market road.

The intersection has a bar and general store / gas station. It also features a multi-car wreck on the side of the road.

### Market District

The District is home to the largest housing unit in Quiet Hill consisting of multiple units, each with multiple floors.

The Market District is home to a school and a church.

### Mining District

The Mining District provides access to the mines, ammunition manufacturing and the blast furnace.

### Chemistry District

Covered in a permanent smog, the chemistry district has tools for processing materials into propellants.

The local volcano is accessible through the chemistry district.

The chemistry district also has a park.

# The Doomer Theory of Economy

Players' main motivation is to acquire ammunition. Since ammunition is money, there is a conflict.

The capacity for people to acquire ammunition depends upon the production capacity in Quiet Hill.

Each of the three houses perform functions necessary to the production of ammunition and players are responsible for keeping them all stocked.

Each house has certain stockpiles which rely on crafting processes and materials that come from other houses.

When a player drops provides a house with resources they are credited based on the current market rates for the material in question. Credit with any particular house may be used to purchase ammunition or exchanged with another house for their house credits.

The games economy is being updated constantly based on the stockpiles and demands on each resource and the price of ammunition and exchange rates between the three houses will fluctuate based on this.
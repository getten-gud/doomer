# Login Logistics

The client does not connect when the game is opened. If there is any checking against the game server, it would be done by the web server and not directly against the socket server.

The user enters their nickname, selects a character, and optionally enters a save code.

The user clicks play, the client requests a map from the server. The administrator account will add a shared secret to the RPC call. 

The server responds by adding the player to the map, communicating the new player to other players on the same map.

The scene is then built and when complete, the 'Ready to Play' screen is displayed until the user clicks play.

Upon clicking play, the user requests a spawn from the server, which will place the user and communicate the spawn event to everyone in the same map / section as the player.

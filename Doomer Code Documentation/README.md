# Doomer Code Documentation

The Game Client and Server are made using [Godot](https://godotengine.org/) and so the majority of the code assets are written in [GDScript](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_basics.html), however the API that hosts news and updater service is written in [NodeJS](https://nodejs.org/en/).

The documentation is broken up into separate sections for the [Game Client](/game-client/), the [Game Server](/game-server/), and the [web-server](/web-server/).
# block_references.gd

This class stores and calculates references to UV coordinates on the sprites for the wall, floor, and ceiling textures rendered by the game.

## properties

### blocks

A dictionary object with two keys: 
`materials` which defines the size and tile counts of the atlas textures for the floor, ceiling and walls.
and `tiles` which gives references to the UV coordinates for each tile.

## functions

### _init():

When the block_references is initialized, it calculates UV coordinates for the provided options in `blocks.tiles` using the tilemap data given in `blocks.materials`
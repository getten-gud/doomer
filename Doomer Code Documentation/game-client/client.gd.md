# client.gd

Connects the player to the game server and loads up the map.

## Signals

### connection_failed
### connection_success
### connection_problem
### error

## Properties

### client

### config
Config is an instance of a config_loader to load the  `res://cfg/client.tres` file.

### current_map
**deprecated:** this variable does indeed store the current map but it is not needed anywhere.

### players_by_id
**deprecated:** this variable is likely better served at the map level. Issues arise in places like shotgun.gd where a client needs a list of players to RPC visibility events to.

## Methods

### _ready()
Config is populated by the contents of `res://cfg/client.tres`.

### bad_nickname(msg) *remote*
Disconnects the client and returns the UI to the previous state. The `error` and `connection_problem` signals are emitted.

### websocket_join_server
Connects to the server via the details provided in `res://cfg/client.tres`. Sets the client property to the resulting WebSocketClient.

### join_server
**deprecated: now using websocket_join_server instead.**

### _process(delta)
Websocket clients must manually poll

### _player_connected(character, nickname, code)
RPC the server requesting map data and emits the `"connection_success"` signal for the UI.

### connection_failed
this is called by a WebSocketClient signal and in turn calls a signal which resets the UI.

### hide_pregame_ui

Hides the opening interface.

### play_game_button

RPC's the server requesting a spawn and hides the "Play Game" UI element

### init_map(map_data) *remote*

Loads a map scene and inits it with the map data provided instantiates it.
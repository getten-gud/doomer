# Game Client

## Classes

### block_references

> *(see: [block_references.gd](/block_references.gd.md))*

### client

> *(see: [client.gd](/client.gd.md))*

### draw_block

> *(see: [draw_block.gd](/draw_block.gd.md))*

### draw_mesh

> *(see: [draw_mesh.gd](/draw_mesh.gd.md))*

### map

> *(see: [map.gd](/block_references.gd.md))*

### my_player

> *(see: [my_player.gd](/my_player.gd.md))*

### other_player

> *(see: [other_player.gd](/other_player.gd.md))*

### subtitle

> *(see: [subtitle.gd](/subtitle.gd.md))*

### ui

> *(see: [ui.gd](/block_references.gd.md))*

### zombie

> *(see: [zombie.gd](/zombie.gd.md))*

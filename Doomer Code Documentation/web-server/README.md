# DOOMER Web Server

The DOOMER Web server is an application written in NodeJS and hosts an API for the game client and a static website to make requests against.

## Features

- **News and Updates**: Retrieves a list of updates to display to the user ingame or online.
- **Updater Services**: Points standalone users auto-updater to binaries for the most recent version of the client.
# server.gd

## Signals

### server_stopped

### server_started

### add_player(player)

### remove_player(player)

### add_map(map)

### remove_map(map)

### output

## Properties

### name_generator

holds an instance of `name_generator`, used to generate map names.

### r

Holds preload resources to spawnable elements, likely to be deprecated and moved into the `game_map`.

### Map

Holds an instance of `draw_maps`.

### loaded_maps

Array of the maps that the server is running, likely to be deprecated.

### players_connected

Array of all connected players.

### clients_by_id

Dictionary of all player objects keyed to their ID's

### players_by_nickname

Dictionary of all player objects keyed to their nicknames

### websocket

The `WebSocketServer` being used by the server.

## Methods

### _process(delta)

Manual polling must be performed on a `WebSocketServer`.

### start_websocket_server(port)

Starts the server listening on the provided port.

### stop_server()

Stop server listening

### request_map(character, nickname, code) *remote*
The players entered details are checked for validity here.

Called remotely by clients after the user enters their character, nickname and/or code.

determines where the player belongs and adds them to the appropriate map. Also registers the player entity with the `players_by_nickname` property.

### _player_connected(id)

Creates a player instance for the player and registers it with the class properties.

### _player_disconnected()

Calls `disconnect_from_game()` on the disconnected player's object. Calls `remove_player(id)` providing the disconnected player's id.

### remove_player(id)

Erases the player from clients_by_id and players_by_nickname if necessary.

Also calls `remove_player` on the map that the player was located in.

Queue's the player instance for freeing.

### create_new_map()

**This method will be dramatically changed in a future version**
Generates a new map as an empty square.

### assign_player_to_map

**This method will be dramatically changed in a future version**
if there aren't any maps loaded, creates a new map, otherwise, adds the  player to the map with the lowest population.